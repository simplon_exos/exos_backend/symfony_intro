<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;



class ExoController extends AbstractController {

    /**
     * @Route("/exoController", name="exoController")
     */
    public function exoController () {
        $test = rand(1, 100);
        $tab = ['ga', 'zo', 'bu', 'meuh'];
        return $this->render('exo-twig.html.twig', [
            'test' => $test,
            'tab' => $tab
            ]);

    }


}