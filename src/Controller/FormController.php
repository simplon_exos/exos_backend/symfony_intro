<?php

namespace App\Controller;

use App\Entity\Person;
use App\Form\PersonType;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends AbstractController
{
    private $list = ['hello', 'bloup', 'toto'];

    /**
     * @Route("/formController", name="formController")
     */
    public function formController(Request $request, array $list = [])
    {
        $name = $request->get('input-name');
        if ($name) {
            $this->list[] = $name;
        }
        return $this->render('form.html.twig', [
            'name' => $name,
            'list' => $this->list
        ]);
    }

    /**
     * @Route("/form-person", name="form_person")
     */
    public function formPerson(Request $request)
    {
        $person = null;
        $name = $request->get('input-name');
        $personality = $request->get('input-personality');
        $age = $request->get('input-age');
            $personRepo = new PersonRepository();

        if($name && $age && $personality) {
            $person = new Person($name, $personality,$age);
            $personRepo->add($person);
            return $this->redirectToRoute("show_person");

        }
        return $this->render('person.html.twig');
    }

    /**
     * @Route("/form-symfony", name="form_symfony")
     */
    public function formSymfony (Request $request) {
        $form = $this->createForm(PersonType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            dump($form->getData());
        }

        return $this->render('form-symfony.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
