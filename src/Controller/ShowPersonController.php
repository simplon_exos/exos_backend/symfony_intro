<?php

namespace App\Controller;

use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class ShowPersonController extends AbstractController {

    /**
     * @Route("/show-person", name="show_person")
     */
    public function index(PersonRepository $repo){
        $personArray = $repo->findAll();
        return $this->render('show-person.html.twig', [
            'personArray' => $personArray
        ]);
    }

    /**
     * @Route("/person/{id}", name="person_by_id")
     */
    public function personById(Int $id, PersonRepository $repo){
        $personById = $repo->findById($id);
        return $this->render('person-by-id.html.twig', [
            'person' => $personById
        ]);
    }
}