<?php

namespace App\Repository;

use App\Entity\Person;

class PersonRepository
{
    private $pdo;

    public function __construct() {
        
        $this->pdo = new \PDO(
            'mysql:host=' . $_ENV['DATABASE_HOST'] . ';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV['DATABASE_PASSWORD']
        );
    }

    /**
     * Méthode qui va aller chercher toutes les personnes
     * présentent dans la base de données et les convertir
     * en instance de la classe Person
     * @return Person[] les personnes contenues dans la bdd
     */
    public function findAll(): array
    {
        //On crée la requête SQL en utilisant la méthode prepare de pdo
        $query = $this->pdo->prepare('SELECT * FROM person');
        //on exécute la requête
        $query->execute();
        /**
         * On récupère tous les résultats sous forme de tableau de 
         * tableau associatifs
         */
        $results = $query->fetchAll();
        $list = [];
        //On fait une boucle sur les résultats
        foreach ($results as $line) {
            /**
             * A chaque ligne de résultat, on crée une instance de la 
             * classe Person en lui donnant comme arguments les données
             * contenues dans chaque ligne
             */
            $person = $this->sqlToPerson($line);
            //$person = new Person($line['name'], $line['personality'], $line['age'], $line['id']);
            //On met la personne créée dans un tableau
            $list[] = $person;
        }
        //On renvoie le tableau de Person
        return $list;
    }
    /**
     * Méthode permettant de faire persister une instance de Person
     * en base de données
     */
    public function add(Person $person): void {
        /**
         * On crée la requête pour faire une insertion, et dans cette
         * requête pour les valeurs à insérer dans la bdd, on va plutôt
         * mettre des placeholders (:name, :personality ...) qu'on
         * pourra assigner par la suite
         */
        $query = $this->pdo->prepare('INSERT INTO person (name,age,personality) VALUES (:name,:age,:personality)');
        //On assigne chaque valeur à faire persister venant de l'instance
        //de la classe Person aux placeholders définis dans la requête.
        //On peut également précisé en troisième argument le type SQL attendu
        $query->bindValue('name', $person->getName(), \PDO::PARAM_STR);
        $query->bindValue('age', $person->getAge(), \PDO::PARAM_INT);
        $query->bindValue('personality', $person->getPersonality(), \PDO::PARAM_STR);
        //On exécute la requête
        $query->execute();
        //On assigne l'id généré par SQL à l'instance de la Person
        $person->setId(intval($this->pdo->lastInsertId()));
    }

    /**
     * Méthode permettant de récupérer une personne spécifique en utilisant
     * son id. Si la personne n'existe pas, on renvoie null
     */
    public function findById(int $id): ?Person {
        //On fait la requête SELECT mais avec un WHERE pour l'id cette fois
        $query = $this->pdo->prepare('SELECT * FROM person WHERE id=:idPlaceholder');
        //On assigne au placeholder la valeur contenu dans l'argument
        //id de la méthode
        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        //On exécute la requête
        $query->execute();
        //On récupère le premier résultat de la requête
        $line = $query->fetch();
        //Si ce résultat existe bien
        if($line) {
            //Alors on renvoie l'instance de Person correspondante
            return $this->sqlToPerson($line);
            //return new Person($line['name'], $line['personality'], $line['age'], $line['id']);
        }
        //Sinon on renvoie null pour indiquer qu'aucune personne ne
        //correspondait à l'id fourni
        return null;

    }
    /**
     * Methode dont le but est de transformer une ligne de résultat
     * PDO en instance de la classe Person
     * Cette méthode est juste là dans un but de refactorisation afin
     * d'éviter la répétition dans les différents find
     */
    private function sqlToPerson(array $line):Person {
        return new Person($line['name'], $line['personality'], $line['age'], $line['id']);
    }
}
