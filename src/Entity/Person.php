<?php

namespace App\Entity;

class Person {
    private $id;
    private $name;
    private $personality;
    private $age;

    public function __construct(string $name ='', string $personality='', int $age=0, int $id = null) {
        $this->id = $id;
        $this->name = $name;
        $this->personality= $personality;
        $this->age = $age;
    }

    public function getName():string {
        return $this->name;
    }
    public function setName(string $name) {
        $this->name = $name;
    }
    public function getAge():int {
        return $this->age;
    }
    public function setAge(string $age) {
        $this->age = $age;
    }
    public function getPersonality():string {
        return $this->personality;
    }
    public function setPersonality(string $personality) {
        $this->personality = $personality;
    }

    public function getId():int {
        return $this->id;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }
}